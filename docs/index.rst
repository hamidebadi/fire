.. Fire documentation master file, created by
   sphinx-quickstart on Sun Aug 26 22:01:37 2012.

Welcome to Fire's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   setup
   usage
   dev
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

