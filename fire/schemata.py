""" schemata.py

Contains Colander schemata for validating form/json data.

Note: there is also the file forms.py containing colander schema but
some of those schema are designed in a way that makes it very difficult to
even import this module cleanly in a unit test. Until fire.forms is refactored,
new, tested schema can be created here.

"""

import colander
import deform


# ~~~ Custom Colander Types ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class StringLower(colander.String):
    """ Defines a new colander types that inherits from string
    but convert the value to lower case when deserializing """

    def deserialize(self, node, cstruct):
        s = super(self.__class__, self).deserialize(node, cstruct)
        return s and s.lower()


# ~~~ Custom Colander Nodes ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class CourseCodeNode(colander.SchemaNode):
    """ Custom node for course code. Both the validator and the widget
    are marked as defered: they have to be bound to a list of custom
    object when the schema is used. """
    schema_type = lambda self: colander.String()

    @colander.deferred
    def widget(node, kw):
        values = [(cc, cc) for cc in kw['course_codes']]
        return deform.widget.SelectWidget(values=values)

    @colander.deferred
    def validator(node, kw):
        values = [cc for cc in kw['course_codes']]
        return colander.OneOf(values)


class PersonNummerNode(colander.SchemaNode):
    """ Custom schema node for personnummer to do custom validation
    and proparation """

    schema_type = lambda self: colander.String()
    validator = colander.Regex(r'^[0-9a-zA-Z]{10}$',
                               msg='Must be in the format YYMMDDNNNN')

    def preparer(self, value):
        """ Prepare value: remove '-' and century """
        if not value:
            return value
        value = value.replace('-', '')
        if len(value) == 12 and value.startswith('19'):
            value = value[2:]
        return value


# ~~~ Schemata ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class RegisterSchema(colander.MappingSchema):
    """ Registration form: just require an email, more details will be
    requested once the email is validated, using the student setting form.
    Note the use of colander.Email to validate the address. """

    signup_email = colander.SchemaNode(StringLower(),
                                       title="Email address",
                                       validator=colander.Email())


class PasswordResetSchema(colander.MappingSchema):
    """ Password reset form: just require an email, the link to the actual
    reset will be sent there. """

    reset_email = colander.SchemaNode(StringLower(),
                                      title="Email address",
                                      validator=colander.Email())


class StudentSettingsSchema(colander.MappingSchema):
    """ Used to allow students to enter their information when registering
    and to change them later. """

    first_name = colander.SchemaNode(colander.String(),
                                     validator=colander.Length(min=1),
                                     title='First name')

    last_name = colander.SchemaNode(colander.String(),
                                    validator=colander.Length(min=0),
                                    title='Last name')

    id_number = PersonNummerNode(title='Personnummer')

    course = CourseCodeNode(title='Course code')


class StudentCreateAccountSchema(StudentSettingsSchema):
    """ Schema for the student registration page (after email verification)
    Inherit from StudentSettingsSchema and just add the password """

    password = colander.SchemaNode(colander.String(),
                                   validator=colander.Length(min=12),
                                   widget=deform.widget.PasswordWidget(),
                                   title='password')

    confirm_password = colander.SchemaNode(colander.String(),
                                           widget=deform.widget.PasswordWidget(),
                                           title='confirm password')

    def validator(self, node, cstruct):
        if cstruct['password'] != cstruct['confirm_password']:
            exc = colander.Invalid(node)
            exc['confirm_password'] = 'Must match password above.'
            raise exc
