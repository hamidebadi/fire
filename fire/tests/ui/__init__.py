from threading import Thread
from time import sleep
from tempfile import mkdtemp
import os
import shutil
from unittest import TestCase
from urlparse import urljoin

from selenium.webdriver import Firefox
from wsgiref.simple_server import make_server
from sqlalchemy import create_engine

import fire
from fire.models import DBSession, Base

DATADIR = mkdtemp()
PORT = 6543
SLUG = 'test'

SETTINGS = {}
SETTINGS['fire.auth_tkt_secret']  = 'test'
SETTINGS['fire.course_slug']      = SLUG
SETTINGS['fire.data_dir']         = DATADIR
SETTINGS['mail.queue_path']         = os.path.join(DATADIR,'mailq')
SETTINGS['mail.default_sender'] = 'noreply@fire.cs.chalmers.se'

def setUp():
    """ Package setup function:
    starts the app server for the selenium tests """
    print "Starting server... (data: %s)" % DATADIR
    global server
    server = make_server('', PORT, fire.main(None, **SETTINGS))
    Thread(target=server.serve_forever).start()
    sleep(2)

def tearDown():
    """ Stop the fire thread """
    global server
    print "Stoping server"
    if server:
        server.shutdown()
    shutil.rmtree(DATADIR)

def mkurl(path):
    """Method to create an absolute url for the test server
    >>> testurl("login")
    "http://localhost:8521/test/login"
    """
    return urljoin("http://localhost:%d/%s/" % (PORT, SLUG), path)


class LiveTestCase(TestCase):
    def setUp(self):
        # Reset the database...
        Base.metadata.drop_all(DBSession.bind)
        Base.metadata.create_all(DBSession.bind)
        # Close the session after each test
        self.addCleanup(DBSession.remove)
        # Setup selenium driver
        self.driver = Firefox()
        self.driver.implicitly_wait(10)
        # close the browser after the test
        self.addCleanup(self.driver.close)

    def tearDown(self):
        # Take a screenshot at the end of each test
        self.driver.get_screenshot_as_file(self.id() + '.png')

