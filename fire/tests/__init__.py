import unittest
import transaction
from pyramid import testing
from fire.models import DBSession

def _initTestingDB():
    from fire.models import DBSession
    from fire.models import Base
    from sqlalchemy import create_engine
    engine = create_engine('sqlite://')
    DBSession.configure(bind=engine)
    session = DBSession()
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    return session


class DBTestCase(unittest.TestCase):
    def setUp(self):
        self.session = _initTestingDB()
        self.trans = transaction.begin()
        testing.setUp()

    def tearDown(self):
        transaction.abort()
        testing.tearDown()
        #DBSession.close_all()
        DBSession.remove()


    def save(self, *objs):
        self.session.add_all(objs)
        self.session.flush()

    def _add_student(self, id=u'userid'):
        from fire.models import Student
        u = Student(id)
        u.password = u'password' 
        u.first_name = 'John'
        u.last_name = 'Doe'
        self.save(u)
        return u

