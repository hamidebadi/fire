import unittest, os
from unittest import TestCase

from nose.tools import eq_, ok_
from mock import MagicMock, patch, call

from pyramid import testing

from . import DBTestCase
from fire.views.admin import AdminGradersView
from fire.views.signup import SignupView
from fire.models import Student, Grader, EmailVerification


class TestAddGrader(DBTestCase):
    """ Test the AdminGradersView.add_grader view: """

    def setUp(self):
        super(TestAddGrader, self).setUp()
        self.request = MagicMock()
        # Make sure that request.params['newgrader'] returns a value:
        self.request.params.__getitem__.return_value = 'John Doe <test@test.com>'
        self.request.route_url.return_value = '/'
        self.request.registry.settings.__getitem__.return_value = \
            os.environ['TOX_ENVTMPDIR']

    def test_it_creates_a_grader_in_the_database(self):
        """ test that a Grader is created in the database """
        agv = AdminGradersView(self.request)
        agv.add_grader()
        g = self.session.query(Grader).get('test@test.com')
        self.assertIsNotNone(g)

class TestSignupStep1(DBTestCase):
    """ Test the SignupView.step1 view: """

    def setUp(self):
        super(TestSignupStep1, self).setUp()
        self.request = MagicMock()
        self.request.user = None
        self.request.route_url.return_value = '/'
        self.request.registry.settings.__getitem__.return_value = \
            os.environ['TOX_ENVTMPDIR']

    def test_it_creates_token_in_the_database(self):
        """ An verification token should be created in the database """
        with patch('fire.views.signup.make_form') as make_form:
            make_form.return_value.validate.return_value = {'signup_email':'test@test.com'}
            from fire.models import EmailVerification
            agv = SignupView(self.request)
            agv.step1_email()
            tokens = self.session.query(EmailVerification).\
              filter( EmailVerification.email == 'test@test.com').\
              all()
            self.assertEquals(1, len(tokens))

@patch('fire.models.EmailVerification.check')
class TestSignupStep2(DBTestCase):
    """ Test the SignupView.step2 view """

    def setUp(self):
        super(self.__class__, self).setUp()
        self.request = MagicMock()
        EmailVerification.create(
                email = 'test@test.com',
                token = 'aaaabbbbccccdddd' )

    @patch('fire.views.signup.make_form')
    def test_invalid_token(self, make_form, check_m):
        """ When passed an invalid token, the view return a dict with an error
        message """
        check_m.side_effect = ValueError('Invalid verification token')
        ret = SignupView(self.request).step2_verification()
        self.assertEquals( dict( error = 'Invalid verification token'), ret )

    @patch('fire.views.signup.make_form')
    def test_valid_token(self, make_form, check_m):
        """ When passed a valid token, the view returns a dictionary with the
        validated email to be passed to the template """
        check_m.return_value = 'test@test.com'
        ret = SignupView(self.request).step2_verification()
        self.assertEquals(
                dict( email = 'test@test.com', form = make_form.return_value),
                ret )

    @patch('fire.views.signup.make_form')
    def test_old_token(self, _, check_m):
        """ When passed a (valid) token for an address that has already been
        validated, the view returns a dict with an error """
        with patch('fire.models.User.get') as mock:
            # now User.get(...) will not be None
            ret = SignupView(self.request).step2_verification()
            self.assertEquals( dict( error = 'Email already validated'), ret )
