from pyramid.view import view_config, view_defaults
from pyramid.httpexceptions import HTTPFound
from deform import ValidationFailure

from fire.models import *
from ..schemata import PasswordResetSchema
from ..forms import make_form_message, make_form, PasswordSchema
from .. import mail


@view_defaults(
    route_name='password_reset',
    permission='__no_permission_required__')
class PasswordResetView(object):
    def __init__(self, request):
        self.request = request

        self.password_reset_form = make_form(
            PasswordResetSchema(),
            buttons=('reset password',),
            action=self.request.route_path('password_reset'),
            formid='resetpwdform',
            use_ajax=True
        )

    @view_config(
        renderer='genshistream',
        request_method='POST',
        request_param='reset_email',
        check_csrf=True)
    def send_reset_pwd_link(self):
        if self.request.user:
            msg = "You are logged in. Please use password change feature."
            self.request.flash_fire(type='error', msg=msg)
            return HTTPFound(location=self.request.route_path('user_password'))

        try:
            data = self.password_reset_form.validate(self.request.POST.items())
        except ValidationFailure, e:
            # TODO: ValidationFailure.render does not accept the same arguments as
            # Form.render does. So we don't get the nice form on error.
            # return e.render(css_class='span4 well', show_labels=False)
            return e.render()

        email = data['reset_email']

        if User.get(email) is not None:
            reset_code = PasswordReset.create(email=email)
            reset_url = self.request.route_url('password_reset', _query={'token': reset_code.token})
            mail.sendmail(self.request, [email],
                          'Password reset for your Fire account',
                          'fire:templates/mails/password_reset.txt',
                          dict(url=reset_url, course_name=Settings.get('course_name')))

        msg = "A password reset link has been sent to %s if you previously registered under this email." % email
        return make_form_message(type='success', msg=msg, formid="resetpwdform")

    @view_config(
        renderer='fire:templates/password_reset.html',
        request_method='GET',
        request_param='token',
        check_csrf=False)
    def reset_password(self):
        if self.request.user:
            msg = "You are logged in. Please use password change feature."
            self.request.flash_fire(type='error', msg=msg)
            return HTTPFound(location=self.request.route_path('user_password'))

        token = self.request.params['token']

        create_password_form = make_form(
            PasswordSchema(),
            buttons=('save',),
            action=self.request.route_url('password_reset', _query={'token': token}),
            formid='password_reset_form'
        )

        reset = PasswordReset.get(token)

        if reset is not None:
            return dict(email=reset.email, form=create_password_form)
        else:
            return dict(error="Invalid password reset token.")

    @view_config(
        renderer='fire:templates/password_reset.html',
        request_method='POST',
        request_param='token',
        check_csrf=True)
    def set_new_pwd(self):
        if self.request.user:
            msg = "You are logged in. Please use password change feature."
            self.request.flash_fire(type='error', msg=msg)
            return HTTPFound(location=self.request.route_path('user_password'))

        token = self.request.params['token']

        create_password_form = make_form(
            PasswordSchema(),
            buttons=('save',),
            action=self.request.route_url('password_reset', _query={'token': token}),
            formid='password_reset_form'
        )

        reset = PasswordReset.get(token)

        if reset is None:
            return dict(error="Invalid password reset token.")

        try:
            data = create_password_form.validate(self.request.POST.items())
        except ValidationFailure, e:
            return dict(form=e)

        user = User.get(reset.email)
        user.password = data['password']
        DBSession.add(user)
        DBSession.delete(reset)

        msg = "Your password has been updated. You can now use it to log in."
        self.request.flash_fire(msg=msg)
        return HTTPFound(location=self.request.route_path('login'))
