import os
import codecs
import tarfile
import tempfile
from datetime import datetime

from pyramid.view import view_config, forbidden_view_config, notfound_view_config
from pyramid.security import remember, forget
from pyramid.httpexceptions import HTTPFound, HTTPGone, HTTPForbidden, HTTPNotFound
from pyramid.response import FileResponse, FileIter, Response
import deform

from .. import settings
from ..models import User, FileCap, CourseCode
from ..filestore import (image_extensions, browsable_extensions)
from ..forms import (make_form,
                     RegisterSchema,
                     PasswordResetSchema,
                     StudentSettingsSchema,
                     GraderSettingsSchema,
                     PasswordSchema)

import logging

log = logging.getLogger(__name__)


@view_config(route_name='root', permission='__no_permission_required__')
def root(request):
    return HTTPFound(location=request.route_path('login'))


@view_config(route_name='login', renderer='fire:templates/login.html',
             permission='__no_permission_required__')
def login(request):
    post_data = request.POST
    log.debug('start login: %r', post_data)
    if 'email' in post_data:
        log.debug('Attempting login')
        email = post_data['email']  # will be used as the user id, not necessarily email
        passwd = post_data['password']
        if User.check_password(email, passwd):
            u = User.get(email)
            u.last_login = datetime.now()
            log.debug('logged in, setting headers')
            if 'remember' in post_data:
                headers = remember(request, email, max_age=settings.get(request, 'login_lifetime'))
            else:
                headers = remember(request, email)
            request.session.invalidate()
            return HTTPFound(location=request.route_path('root'), headers=headers)
        else:
            log.debug('login failed')
            headers = forget(request)
            request.session.invalidate()
            msg = 'Incorrect user name (email) or password. If you forgot your password, use the form below.'
            request.flash_fire(type='error', msg=msg)
            return HTTPFound(location=request.route_path('login'), headers=headers)

    # We use the chance here and issue a *new* fresh csrf token.
    # Open windows on this session will fail anyways since a login
    # success or failure both invalidate the current session.
    request.session.new_csrf_token()

    signupform = make_form(
        RegisterSchema(),
        buttons=('register',),
        action=request.route_path('signup'),
        formid='signupform',
        use_ajax=True
    )

    resetpwdform = make_form(
        PasswordResetSchema(),
        buttons=('reset password',),
        action=request.route_path('password_reset'),
        formid='resetpwdform',
        use_ajax=True
    )

    return dict(signupform=signupform, resetpwdform=resetpwdform)


@view_config(route_name='logout', permission='__no_permission_required__')
def logout(request):
    headers = forget(request)
    request.session.invalidate()
    return HTTPFound(location=request.route_path('login'), headers=headers)


@view_config(route_name='get_filecap',
             renderer='fire:templates/file_browse.html',
             permission='__no_permission_required__')
def get_filecap(request):
    if request.user is not None and not settings.get(request, 'debug'):
        return HTTPForbidden('Filecaps cannot be opened under user credentials (server misconfiguration).')
    fc = FileCap.get_or_404(request.matchdict['filecap_id'])
    filename = request.matchdict['filename']
    if filename != fc.filename:
        return HTTPNotFound()
    if fc.expires and fc.expires < datetime.now():
        return HTTPGone()  # 410
    # TODO store and check ip address
    if fc.is_archive:
        # Consider switching this to NamedTemporaryFile and avoid useing FileIter
        tmpfile = tempfile.TemporaryFile()
        archive = tarfile.open(mode='w:gz', fileobj=tmpfile)
        for fullpath, archivepath in fc.contents:
            assert os.path.isfile(fullpath)
            archive.add(fullpath, archivepath)
        archive.close()
        size = tmpfile.tell()
        tmpfile.seek(0)
        resp = Response(content_type='application/x-gzip')
        resp.headers['Content-Disposition'] = "attachment; filename=%s.tar.gz" % str(fc.filename)
        resp.headers['Content-Length'] = str(size)
        resp.app_iter = FileIter(tmpfile)
        return resp
    else:
        resp = FileResponse(fc.contents, request)
        dl = len(request.params.getall("dl")) > 0
        if dl:
            # Don't specify a filename, as encoding it correctly here is fubar.
            # Instead, the browser will guess a correct filename from the url.
            resp.headers['Content-Disposition'] = "attachment"
            return resp
        else:
            extension = os.path.splitext(filename)[1].lower()

            if extension not in browsable_extensions:
                return HTTPNotFound()
            elif extension in image_extensions:
                # TODO check that the file header matches the extension,
                # TODO if not -- suspicious, and respond as an attachment
                return resp
            else:
                languages = {'.js': 'javascript', '.html': 'xml', '.xml': 'xml', '.java': 'java', '.hs': 'haskell',
                             '.lhs': 'haskell', '.py': 'python', '.cpp': 'cpp', '.csharp': 'csharp', '.sql': 'mysql',
                             '.erl': 'erlang', '.sh': 'bash', '.scala': 'scala', '.lisp': 'lisp', '.cl': 'lisp',
                             '.clj': 'lisp', '.scm': 'lisp', '.pl': 'perl', '.rb': 'ruby'}

                # TODO: Maybe use some library that tries to detect the encoding?
                # Try to open the file with one of the following encoding, in this order
                # utf_8 codec for some reason has problems recognizing symbols like u'\0xf6' (umlaut o),
                # iso8859_10 is an encoding for Nordic languages and a remedy for this situation
                encodings = "utf_8", "iso8859_10"
                for encoding in encodings:
                    try:
                        with codecs.open(fc.contents, encoding=encoding) as f:
                            contents = f.read()
                        return dict(file_name=filename, file_language=languages.get(extension, 'text'),
                                    file_contents=contents)
                    except ValueError:
                        continue

                # Nothing worked, perhaps it's not a text file at all?
                # Respond as an attachment
                resp.headers['Content-Disposition'] = "attachment"
                return resp


@view_config(route_name='user_profile',
             renderer='fire:templates/user_profile.html',
             permission='is:authenticated')
def user_profile(request):
    if request.user.role == 'student':
        course_codes = [cc.code for cc in CourseCode.get_all()]
        schema = StudentSettingsSchema().bind(course_codes=course_codes)
    else:
        schema = GraderSettingsSchema()
    form = make_form(schema,
                     buttons=('save',),
                     action=request.route_path('update_user_info'),
                     formid='settingsform',
                     use_ajax=True)
    data = {k: getattr(request.user, k)
            for k in ('first_name', 'last_name', 'id_number', 'course')
            if hasattr(request.user, k)}

    return dict(title='Basic info',
                for_line=request.user.name,
                form=form.render(data))


@view_config(route_name='update_user_info',
             renderer='genshistream',
             request_method='POST', check_csrf=True,
             permission='is:authenticated')
def update_user_info(request):
    if request.user.role == 'student':
        course_codes = [cc.code for cc in CourseCode.get_all()]
        schema = StudentSettingsSchema().bind(course_codes=course_codes)
    else:
        schema = GraderSettingsSchema()
    form = make_form(schema,
                     buttons=('save',),
                     action=request.route_path('update_user_info'),
                     formid='settingsform',
                     use_ajax=True)
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return e.render()

    # Update the user
    for k in ('first_name', 'last_name', 'id_number', 'course'):
        if k in data:
            setattr(request.user, k, data[k])

    data = {k: getattr(request.user, k)
            for k in ('first_name', 'last_name', 'id_number', 'course')
            if hasattr(request.user, k)}

    return form.render(data, successmsg='Saved!')


@view_config(route_name='user_password',
             renderer='fire:templates/user_password.html',
             permission='is:authenticated')
def user_password(request):
    form = make_form(PasswordSchema(),
                     buttons=('save',),
                     action=request.route_path('update_user_password'),
                     formid='passwordform')
    return dict(title="Change password",
                form=form.render(errormsg="Note: You will need to log in again with the new password."))


@view_config(route_name='update_user_password',
             renderer='fire:templates/generic_form.html',
             request_method='POST', check_csrf=True,
             permission='is:authenticated')
def update_user_password(request):
    form = make_form(PasswordSchema(),
                     buttons=('save',),
                     action=request.route_path('update_user_password'),
                     formid='passwordform')
    try:
        data = form.validate(request.POST.items())
    except deform.ValidationFailure, e:
        return dict(title="Change password", form=e.render())

    # Change the password and then log out
    request.user.password = data['password']
    r = logout(request)
    request.flash_fire(msg="Your password has been updated, please log in with the new password.")
    return r


@forbidden_view_config(renderer='fire:templates/exception.html')
def forbidden(e, request):
    return dict(title="Access denied",
                message="You are not authorised to view this page.")


@notfound_view_config(renderer='fire:templates/exception.html')
def notfound(e, request):
    return dict(title="Not found",
                message="This page can not be found.")


@view_config(renderer='fire:templates/exception.html',
             context=Exception,
             permission='__no_permission_required__')
def exception(e, request):
    log.exception(e)
    return dict(title="Internal server error",
                message="Internal server error. Please try again later.")
