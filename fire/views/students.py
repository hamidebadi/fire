from datetime import datetime, timedelta

from pyramid.view import view_config
from pyramid.httpexceptions import HTTPNotFound, HTTPFound, HTTPUnauthorized

from ..models import Lab, Submission, Group, DBSession, FileCap
from .. import filestore, templating, settings
from ..errors import UserException, InternalFireError

import logging

log = logging.getLogger(__name__)

import re

@view_config(route_name='student_main',
             renderer='fire:templates/student_main.html',
             permission='has_role:student')
def student_main(request):
    return dict(labs=Lab.get_active())


@view_config(route_name='student_lab',
             renderer='fire:templates/student_lab.html',
             permission='has_role:student')
def student_lab(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))

    if not lab.grader_assignment:
        # Students should not see a submission link to a misconfigured lab
        return HTTPFound(location=request.route_path('student_main'))

    submissions = request.user.submissions_for_lab(lab)

    if lab.individual:
        group = None
        active_member = None

        # Create a new empty submission if none are active
        if not request.user.has_active_submissions(lab):
            new_submission = Submission(lab, request.user)
            DBSession.add(new_submission)
            DBSession.flush()
            submissions.append(new_submission)
    else:
        if submissions:
            # The group the latest submission of this student.
            # Note that in edge cases, this may *not* be the latest
            # submission for that group, if the student has left the group.
            latest_submission = submissions[-1]
            group = latest_submission.group

            # If the student is still an active member of the group owning
            # the latest submission, and no submissions are active (i.e.
            # new, under review or accepted), we create a new empty submission
            # to allow file uploads
            if request.user in group.active_members and not group.has_active_submissions(lab):
                new_submission = Submission(lab, group)
                DBSession.add(new_submission)
                DBSession.flush()
                submissions.append(new_submission)
        else:
            # If there are no submissions, display information about the active group
            group = request.user.group

            # Just viewing this page then creates a new submission.
            # This is perfectly safe, having a submission with status "new"
            # does not count as a formal lab submission, it's just a placeholder for files
            if group is not None and group.enough_members(lab) == 0:
                new_submission = Submission(lab, group)
                DBSession.add(new_submission)
                DBSession.flush()
                submissions.append(new_submission)

        # The user may be viewing a lab with submissions from a previous group
        active_member = group is not None and request.user in group.active_members

    filesets = [filestore.files_for_submission(s) for s in submissions]

    return dict(
        lab=lab,
        submissions=submissions,
        filesets=filesets,
        group=group,
        active_member=active_member,
        current_user=request.user
    )


@view_config(
    route_name='student_lab',
    request_method='POST',
    request_param='withdraw',
    check_csrf=True,
    permission='has_role:student')
def student_withdraw_submission(request):
    """
    View that allow a student to withdraw a submission.
    Note that the view config makes sure that we only call this method
    if on a POST request where there is a parameter called 'withdraw'.
    """
    submission = Submission.get(int(request.params['withdraw']))
    try:
        if not submission.can_be_withdrawn():
            raise UserException("You are not allowed to withdraw this submission")
        submission.withdraw()
        # Here we need to create a new submission to make sure that the
        # user is able to upload files. If we don't do that, for some reason,
        # the user will need to reload the page to see the 'New submission'
        # section.
        DBSession.add(Submission(submission.lab, request.user))
        request.flash_fire(msg='Submission withdrawn')
    except Exception, e:
        request.flash_fire(type='error', msg=str(e))
    return HTTPFound(location=request.route_path('student_lab', lab=submission.lab_id))


@view_config(route_name='student_upload',
             request_method='POST',
             renderer='json',
             permission='has_role:student')
def student_upload(request):
    # TODO check file sizes, name restrictions etc.
    lab = Lab.get(int(request.matchdict['lab']))
    if lab is None:
        return HTTPNotFound()

    if lab.individual:
        submitter = request.user
    else:
        if request.user.group is None:
            raise UserException("You must create or join a group before submitting to this lab.")

        submitter = request.user.group

        if submitter.enough_members(lab) < 0:
            raise UserException("Not enough members in the group! " +
                                "This lab requires at least %d members, but you only have %d" %
                                (lab.min_members, len(submitter.active_members)))

        if submitter.enough_members(lab) > 0:
            raise UserException("Too many members in the group! " +
                                "This lab requires at most %d members, but you have %d" %
                                (lab.max_members, len(submitter.active_members)))

    # TODO: send submission_id in upload request
    submission = submitter.submissions_for_lab(lab)[-1]
    assert submission.status == Submission.Statuses.new

    # TODO find / create submission
    file_name = request.POST['files[]'].filename
    file_data = request.POST['files[]'].file
    try:
        info = filestore.store_file(submission, file_name, file_data)
        return dict(success='ok',
                    tablerow=unicode(templating.filelisting_row(info, browsable=False)))
    except UserException, e:
        # TODO: log it
        return dict(error="Error: " + e.message)
    except InternalFireError, e:
        log.error(e)
        return dict(error="File upload failed")


@view_config(route_name='student_delete_file',
             request_method='POST',
             renderer='json',
             permission='has_role:student')
def student_delete_file(request):
    lab = Lab.get(int(request.matchdict['lab']))
    if lab is None:
        return HTTPNotFound()

    if lab.individual:
        submitter = request.user
    else:
        if request.user.group is None:
            return HTTPNotFound()
        submitter = request.user.group

    # This fetches the new open submission on this lab.
    submission = submitter.submissions_for_lab(lab)[-1]
    assert submission.status == Submission.Statuses.new

    # If this assert fails, the following invariant is not maintained properly
    # by model.py: For a group submission with status new, submission.submitters 
    # should be the active members of submission.group.
    assert request.user in submission.valid_submitters

    filename = request.body

    try:
        filestore.delete_file(submission, filename)
        return dict(success="ok")
    except Exception, e:
        # TODO log it
        return dict(error="The file could not be deleted")


@view_config(route_name='student_download_file',
             permission='has_role:student')
def student_download_file(request):
    lab = Lab.get_or_404(request.matchdict['lab'])
    sub = Submission.get_or_404(request.matchdict['submission'])
    filename = request.matchdict['filename']

    assert sub.lab is lab

    if request.user not in sub.submitters:
        return HTTPUnauthorized()

    fs = filestore.files_for_submission(sub)
    fileinfo = next((f for f in fs['files'] if f.name == filename), None)
    if fileinfo is None:
        return HTTPNotFound()

    fullpath = filestore.fullpath_for_file(sub, filename)
    cap = FileCap.issue_single(fullpath, filename)

    cap_url = settings.get(request, 'url_capserver') + request.route_path('get_filecap', filecap_id=cap.id,
                                                                          filename=cap.filename)

    dl = len(request.params.getall("dl")) > 0
    if dl:
        cap_url += "?dl=1"
    return HTTPFound(cap_url)


@view_config(route_name='student_submit',
             request_method='POST',
             permission='has_role:student')
def student_submit(request):
    lab = Lab.get_or_404(int(request.matchdict['lab']))
    submission = Submission.get_or_404(int(request.POST["submission_id"]))

    if lab.individual:
        if request.user != submission.submitters[0]:
            message = "Trying to submit with submission id %s whereas submitters do not match ('%s' vs '%s')"
            raise InternalFireError(message % (submission.id, request.user, submission.submitters[0]))
    else:
        if request.user.group is None:
            return HTTPNotFound()

        if request.user.group.id != submission.group.id:
            message = "Trying to submit with submission id %s whereas submitters do not match ('%s' vs '%s')"
            raise InternalFireError(message % (submission.id, request.user.group, submission.group))

    if not submission.active:
        raise InternalFireError("Trying to submit with submission id %s when it's not new" % submission.id)

    if request.user not in submission.valid_submitters:
        # If this happens, one of two things can be wrong:
        # 1. A race (two simultaneous requests) between removing a group member
        # and submitting.
        # 2. model.py failed to maintain the invariant that a "new" submission
        #      should always have current active members of the group as its submitters
        #      list.
        raise InternalFireError("Current user attempting submission is not a submitter.")

    submissions = request.user.submissions_for_lab(lab)
    is_first_submission = not [s for s in submissions if s.status is not Submission.Statuses.new]

    # Deadline check
    first_deadline = submission.first_deadline_override or lab.first_deadline
    final_deadline = submission.final_deadline_override or lab.final_deadline

    deadline = first_deadline if is_first_submission else final_deadline

    now = datetime.now()
    if deadline and (now + timedelta(minutes=1)) > deadline:
        request.flash_fire(type='error', msg="Submission failed: The deadline for this lab has passed.")
        return HTTPFound(location=request.route_path('student_lab', lab=lab.id))

    if lab.allow_physical_submissions and request.POST.getone('physical') == 'yes':
        submission.physical_submission = True

    comment = request.POST['comment'] if 'comment' in request.POST else ""
    if comment:
        filestore.store_comment(submission, comment)

    # We are good to go
    submission.submit()
    submission.comment = comment
    return HTTPFound(location=request.route_path('student_lab', lab=lab.id))


@view_config(route_name='student_newgroup',
             request_method='POST', check_csrf=True,
             permission='has_role:student')
def student_newgroup(request):
    if request.user.group is not None:
        request.flash_fire(type='error', msg='Leave the current group before creating a new one.')
    else:
        joincode = request.POST.getone('joincode')
        joincode_normalized = re.sub(r"\s+", "", joincode, flags=re.UNICODE)
        if joincode_normalized:
            g = Group.get_where(Group.joincode == joincode_normalized)
            if len(g) == 0:
                request.flash_fire(type='error', msg="The join code you entered is invalid.")
            else:
                assert len(g) == 1  # join-codes need to be unique
                g[0].add_user(request.user)
                request.flash_fire(type='success', msg=("You are now a member of group %d" % g[0].id))
        else:
            g = Group(request.user)
            DBSession().add(g)
            request.flash_fire(type='success', msg="Successfully created a new group.")

    return HTTPFound(location=request.route_path('student_main'))


@view_config(route_name='student_leavegroup',
             request_method='POST', check_csrf=True,
             permission='has_role:student')
def student_leavegroup(request):
    request.user.group.remove_user(request.user)
    return HTTPFound(location=request.route_path('student_main'))
