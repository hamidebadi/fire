import os
import errno
import uuid
from datetime import datetime
import unicodedata

from . import settings
from . import errors

"""File storage and processing."""

import logging
log = logging.getLogger(__name__)


def ensure_dir_exists(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def sanitize_filename(fn):
    # IE sends absolute paths as file names, so in case there are
    # backslashes, we grab only the last part.
    if '\\' in fn:
        fn = fn.split('\\')[-1]
        assert len(fn) > 0

    fn = fn.replace('\\', '-')   # redundant because of above, but just in case
    fn = fn.replace('/', '-')
    fn = fn.replace(':', '-')
    fn = fn.replace('~', '-')

    return fn


def path_for_submission(submission):
    lab_str = "lab%02d" % submission.lab.id
    submitter_str = (submission.submitters[0].id 
                     if submission.lab.individual
                     else "group%02d" % submission.group.id)
    submission_str = "submission%02d" % submission.number

    path = os.path.join(settings.get_static('data_dir'), 'filestore', lab_str, submitter_str, submission_str)
    return os.path.abspath(os.path.normpath(path))

text_extensions = ['', '.txt', '.pml']
image_extensions = ['.jpg', '.jpeg', '.gif', '.png', '.pdf']
web_extensions = ['.js', '.html']
source_extensions = ['.java', '.hs', '.lhs', '.py', '.cpp', '.csharp', '.sql',
                     '.agda', '.lagda', '.tex', '.erl', '.sh', '.scala', '.xml',
                     '.lisp', '.cl', '.clj', '.scm', '.pl', '.rb']
browsable_extensions = text_extensions + image_extensions + web_extensions + source_extensions


class FileMetadata(object):
    def __init__(self, file_path):
        self._name = os.path.basename(file_path)
        self._path = file_path
        self._size = os.path.getsize(file_path)
        self._utime = datetime.fromtimestamp(os.path.getmtime(file_path))

        extension = os.path.splitext(file_path)[1].lower()
        # For performance reasons we only check for file extension here.
        # More thorough check is performed immediately before browsing.
        self._browsable = extension in browsable_extensions
        self._sensitive = extension in web_extensions

    @property
    def name(self):
        return self._name

    @property
    def size(self):
        return self._size

    @property
    def path(self):
        return self._path

    @property
    def utime(self):
        return self._utime

    @property
    def browsable(self):
        return self._browsable

    @property
    def sensitive(self):
        return self._sensitive


def store_file(submission, file_name, file_data):
    try:
        temp_path = os.path.abspath(os.path.normpath(os.path.join(settings.get_static('data_dir'), 'tmp')))
        ensure_dir_exists(temp_path)
        tmp_fn = os.path.join(temp_path, str(uuid.uuid4()))
        tmp_file = file(tmp_fn, 'wb')

        if isinstance(file_data, str):
            tmp_file.write(file_data)
        elif isinstance(file_data, unicode):
            tmp_file.write(file_data.encode("utf8"))
        else:
            file_data.seek(0)
            while True:
                data = file_data.read(2 << 16)
                if not data:
                    break
                tmp_file.write(data)

        tmp_file.close()

        final_dir = path_for_submission(submission)
        ensure_dir_exists(final_dir)
        fn = sanitize_filename(file_name)
        final_path = os.path.join(final_dir, fn)

        # if os.path.exists(final_path):
        #     raise errors.UserException("This file already exists!")

        os.rename(tmp_fn, final_path)
        return FileMetadata(final_path)
    except errors.UserException, e:
        raise e
    except Exception, e:
        raise errors.InternalFireError(e)


def delete_file(submission, file_path):
    d = path_for_submission(submission)
    full_path = os.path.join(d, file_path)

    if not os.path.exists(full_path):
        log.error("attempted delete of non-existing file %s", full_path)
        raise errors.InternalFireError("delete_file: File does not exist")

    ensure_dir_exists(os.path.join(d, '.deleted'))

    now = datetime.now()

    del_path = os.path.join('.deleted', now.strftime('%Y%m%d%H%M%S-') + file_path)

    os.rename(full_path, os.path.join(d, del_path))


fire_comment_file = 'FIRE_COMMENT.txt'


def store_comment(submission, comment):
    store_file(submission, fire_comment_file, comment)


def files_for_submission(submission):
    """Returns a dict with two lists, files and deleted_files.
    The first list contains FileMetadata objects, the second dicts with the
    following keys:
     - name : The base name of the file
     - path : The path to the actual file, relative to the submission dir
     - size : The size of the file in bytes, according to the filesystem
    """
    d = path_for_submission(submission)

    if not os.path.isdir(d):
        return dict(files=[], deleted_files=[])

    def file_info(file_path):
        # OS-X uses a different normalization form than most others.
        # Normalize to NFC
        if isinstance(file_path, unicode):
            file_path = unicodedata.normalize('NFC', file_path)
        basename = os.path.basename(file_path)
        return dict(name=basename,
                    path=basename,
                    size=os.path.getsize(file_path),
                    utime=datetime.fromtimestamp(os.path.getmtime(file_path)))

    # IMPORTANT
    # os.listdir returns the Unicode versions of files
    # if the path is itself unicode
    # (see https://docs.python.org/2/howto/unicode.html#unicode-filenames)
    files = [FileMetadata(os.path.join(d, p))
             for p in os.listdir(unicode(d))
             if not p.startswith('.') and p != fire_comment_file]

    files.sort(key=lambda i: i.name)
    
    deleted_dir = os.path.join(d, '.deleted')
    if os.path.isdir(deleted_dir):
        # IMPORTANT
        # os.listdir returns the Unicode versions of files
        # if the path is itself unicode
        # (see https://docs.python.org/2/howto/unicode.html#unicode-filenames)
        deleted_files = [file_info(os.path.join(deleted_dir, p))
                         for p in os.listdir(unicode(deleted_dir))
                         if not p.startswith('.')]
        for info in deleted_files:
            dt, fn = info['name'].split('-', 1)
            info['path'] = os.path.join('.deleted', info['name'])
            info['deleted'] = datetime.strptime(dt, '%Y%m%d%H%M%S')
            info['name'] = fn
        deleted_files.sort(key=lambda i: (i['name'], i['deleted']))
    else:
        deleted_files = []

    return dict(files=files, deleted_files=deleted_files)


def fullpath_for_file(submission, file_path):
    sp = path_for_submission(submission)
    path = os.path.join(sp, file_path)
    return path if os.path.isfile(path) else None
