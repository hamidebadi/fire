$ ->
  hash = window.location.hash
  if hash.length
    activetab = $('#labtabs a[href="' + hash + '"]')
  else
    activetab = $("#labtabs a:first")
  activetab.tab("show")

  updateDefaultRows = () ->
    $('table.table tbody:has(tr.default-row)').each (idx,tb) ->
      empty = $('tr:visible:not(.default-row)', tb).length == 0
      $('tr.default-row', tb).toggle(empty)
  updateDefaultRows()

  updateHiddenRows = () ->
    show = $('#show-all-graders').hasClass('active')
    $('tr.other-grader').toggle(show)
    updateDefaultRows()

  $('#labtabs a').click -> 
    setTimeout updateDefaultRows, 0

  $('#show-all-graders').click () ->
    setTimeout updateHiddenRows, 0
