define ["jquery", "jquery.fileupload"], ($) ->
  $ ->
    $('[data-toggle=tooltip]').tooltip()

    activeUploads = 0

    $("#fileupload").fileupload
      dataType: "json"
      add: (e, data) ->
        ++activeUploads
        $('#submit-button').button('loading')
        data.submit()
      submit: (e, data) ->
        tr = $('<tr/>')
        td = $('<td class="filename"/>')
        td.innerHTML = data.files[0].name
        tr.append(td)
        tr.append('<td colspan="4" class="message"><div class="progress"><div class="bar" style="width: 0%;"></div></div></td>')
        data.tableRow = tr
        $('#filelisting tbody').append(tr)
      done: (e, data) ->
        if data.result.error
          $('.message', data.tableRow).addClass('message-error').text(data.result.error)
        else
          newrow = $(data.result.tablerow)
          setupFileActions(newrow)
          $(data.tableRow).replaceWith(newrow)
          if --activeUploads == 0
            $('#submit-button').button('reset')
      fail: (e, data) ->
        $('.message', data.tableRow).addClass('message-error').text("Upload failed: " + data.jqXHR.statusText)
      progress: (e, data) ->
        progress = parseInt(data.loaded / data.total * 100, 10);
        bar = $(".progress .bar", data.tableRow).css('width', progress + '%')
        if progress >= 95
          bar.addClass("progress-striped active") # things hang a bit at 100%, show that its not frozen

    $('#browse-button').click () ->
      $('#fileupload').click()

    $('.modal-cancel').click () ->
      $(this).closest('.modal').modal('hide')

    setupFileActions = (tr) ->
      $tr = $(tr)
      $('.fileaction', tr).click () ->
        action = $(this).data('action')
        filename = $tr.data('filename')
        switch action
          when "delete"
            mdl = $('#modal-confirm-delete')
            mdl.find('#confirm-delete-filename').text(filename)
            mdl.find('#confirm-delete-button').unbind('click').click () ->
              mdl.modal('hide')
              url = $(this).data('url')
              $.post url, filename, (data) ->
                if data.error
                  alert(data.error)
                else
                  tbody = $tr.parent()
                  $tr.slideUp -> $tr.remove()
            mdl.modal()
            break
          when "download"
            subid = $tr.closest('.submission').data('submission')
            window.location.href += "/submissions/" + subid + "/files/" + filename + "?dl=1";

    $('.filelisting-5col tr').each (idx, tr) ->
      setupFileActions(tr)
    
    $('#submit-button').click () ->
      mdl = $('#modal-confirm-submit')
      # TODO add extra info to modal, e.g. if there are no files etc.
      $('input[name=comment]').val($('#submission-comment').val())
      p = $('#physical_submission')
      if p.length and p.is(':checked')
        $('input[name=physical]').val('yes')
      mdl.modal()
