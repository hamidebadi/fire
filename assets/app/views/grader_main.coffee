class SubmissionsGraph
  constructor: (@canvas) ->
    @url = $(@canvas).data('source')
    @colorAll = $(@canvas).data('color-all')
    @colorMe  = $(@canvas).data('color-me')
    @context = @canvas.getContext("2d")
    console.log("Drawing bars from #{@url} with colors #{@colorAll} and #{@colorMe}")
    @refresh()

  barWidth: 12;
  pxPerUnit: 5;

  drawBar: (index,value) ->
    @context.fillRect(
      @canvas.width - (index + 1) * (@barWidth + 1),  # x
      @canvas.height - value * @pxPerUnit,            # y
      @barWidth,                                      # width
      value * @pxPerUnit)                             # height

  draw: (data) ->
    @context.fillStyle = @colorAll
    for count, i in data.all
      @drawBar(i, count)
    @context.fillStyle = @colorMe
    for count, i in data.me
      @drawBar(i, count)

  refresh: () ->
    $.ajax({
      url: @url,
      dataType: "json",
      success: $.proxy(@draw,@) })

$ ->
  console.log("abcd") 
  new SubmissionsGraph(cva) for cva in $(".bars");
